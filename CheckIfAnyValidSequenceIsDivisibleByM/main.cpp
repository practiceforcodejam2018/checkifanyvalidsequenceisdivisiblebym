#include <iostream>
#include <vector>

using namespace std;

class Engine
{
    private:
    bool status = false;
        bool combineAllPossibilitiesAndCheckIfItIsDivisibleByNumber(vector<int> listVector , int currentIndex , int currentSum , int size , int moduloNumber)
        {
            if(status)
            {
                return true;
            }
            if(currentIndex == size)
            {
                cout<<"Sum = "<<currentSum<<endl;
                if(!(currentSum % moduloNumber))
                {
                    status = true;
                    return true;
                }
                return false;
            }
            combineAllPossibilitiesAndCheckIfItIsDivisibleByNumber(listVector , currentIndex + 1 , currentSum + listVector[currentIndex] , size , moduloNumber);
            combineAllPossibilitiesAndCheckIfItIsDivisibleByNumber(listVector , currentIndex + 1 , currentSum - listVector[currentIndex] , size , moduloNumber);
            return status;
        }
    
    public:
        bool checkIfListVectorMakesTheCombinationWhichIsDivisibleByNumber(vector<int> listVector , int moduloNumber)
        {
            int listVectorSize = (int)listVector.size();
            return combineAllPossibilitiesAndCheckIfItIsDivisibleByNumber(listVector , 0 , 0 , listVectorSize , moduloNumber);
        }
};

int main(int argc, const char * argv[])
{
    vector<int> listVector   = {1 , 3 , 9};
    int         moduloNumber = 5;
    Engine      e            = Engine();
    e.checkIfListVectorMakesTheCombinationWhichIsDivisibleByNumber(listVector , moduloNumber) ? cout<<"YES"<<endl : cout<<"NO"<<endl;
    return 0;
}
